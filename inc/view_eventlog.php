<div class="jumbotron jumbotron-fluid bg-light">
<div class="container">
<h1 class="display-3">Main Dashboard</h1>
<p class="lead">Quick View at Security System</p>
</div>
</div>

<div class="jumbotron jumbotron-fluid bg-light">
  <div class="container">
    <h1 class="display-5">View All EventLogs</h1>
  </div>
</div>
<div class = "container-sm">
<div class="input-group mb-3 text-center">

  <script>
  $( function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  </script>

<input title="Enter Timestamp you wish to find." type="text" class="form-control" placeholder="Search EventLogs by Timestamp" aria-label="Search Eventlogs by Timestamp" id="datepicker" aria-describedby="button-addon2" method="post">

</div>
</div>
<div class='col mb-4' style='margin-top: 15px'>
<div class='card'>
<div class='table-responsive-sm'>
<table class='table' id="autodata">

</table>
</div>
</div>
</div>
<script>
$("#autodata").load("inc/getdata.php"); // inital load

var datepicker = ""; // placeholder 
$(document).ready(function() {
 setInterval(function() { //runs this code after set interval
      if($("#datepicker").val() === "") { //if search data is empty
      $("#autodata").load("inc/getdata.php?datepicker="+datepicker);    //loads page

     } else {
$(document).ready(function(){
  $("#datepicker").keyup(function(){ //runs code on keyup
	  datepicker = $('#datepicker').val() //sets search to #search_data input
    $("#autodata").load("inc/getdata.php?datepicker="+datepicker);//loads data with search criteria 
  });
});
     }
}, 1000);//interval in ms
}); 
</script>