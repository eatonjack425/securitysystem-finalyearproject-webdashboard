<?php
$db = "SecurityDashboardDB";
$servername = "localhost";
$username = "root";
$password = "";
$search;

// Create connection
$conn = new mysqli($servername, $username, $password, $db);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT eventLogID, timestamp, imagePath FROM eventlogs";
    
if(isset($_GET["datepicker"]) ){
    $search = htmlspecialchars($_GET["datepicker"]);
    if ($_GET["datepicker"] != "")
    {
        $sql = $sql ." WHERE DATE(timestamp) = '".$search."'";
    }
}

$sql = $sql ." ORDER BY timestamp DESC;";

$results = $conn->query($sql);

echo '<thead class="thead-info">
    <tr>
      <th title="Timestamp of EventLog" scope="col">Timestamp</th>
    </tr>
  </thead>';
if ($results->num_rows > 0) {
while($row = $results->fetch_assoc()) {
      echo 
        '<tbody>
      <tr>
      <th scope="row" title="Click to go to EventLog information"><a href="master.php?page=EventLogView&id='.$row["eventLogID"].'">'.$row["timestamp"].'</a></th>
    </tr>
    </tbody>';
}
}
$conn->close;
?>