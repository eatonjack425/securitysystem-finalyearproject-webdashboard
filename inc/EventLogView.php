<!-- Temp DB Connection and Mangement -->
<?php
function BitToBoolean($bit) {
    if($bit == 0)
        $boolean = "False";
    else $boolean = "True";
    return $boolean;
}
function BitToYesNo($bit) {
    if($bit == 0)
        $boolean = "No";
        else $boolean = "Yes";
        return $boolean;
}

$db = "SecurityDashboardDB";
$servername = "localhost";
$username = "root";
$password = "";
$search = $_GET['id'];


// Create connection
$conn = new mysqli($servername, $username, $password, $db);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT timestamp, imagePath FROM eventlogs WHERE eventLogID = ".$search;

$results = $conn->query($sql);

while ($row = $results->fetch_assoc())
{
    $timestamp = $row['timestamp'];
    $imagePath = $row['imagePath'];
}

?>



  <!-- Page Header -->
  <script src="face-api.min.js"></script> 
<script src="script.js"></script> 
    <div class='jumbotron jumbotron-fluid'>
  <div class='container'>
    <h1 class='display-4'>Event Log View</h1>
    <p class='lead'>More Detailed View of an Event Log </p>
  </div>
</div>
<div class='container-fluid'>
<div class='row'>
  <div class='col-sm-6'>
    <div class='card' style='margin-top: 5px'>
      <div class='card-body'>
         <h5 class='card-title'>Details</h5>
        <p class='card-text'>Timestamp: <?php echo $timestamp?></p>
        <p class='card-text' id='imageSelect1'>Path: <?php echo $imagePath?></p>
<img src='<?php echo $imagePath?>' class='card-img-top' id='imageSelect'>
    </div>
  </div>
    </div>
</div>
</div>
