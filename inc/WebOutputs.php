<?php
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $action = test_input($_GET["action"]);
    if ($action == "outputs_state") {
        $ipaddress = test_input($_GET["ipaddress"]);
        $result = getAllOutputStates($ipaddress);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $rows[$row["gpio"]] = $row["state"];
            }
        }
        echo json_encode($rows);
        $result = getBoard($ipaddress);
        if($result->fetch_assoc()) {
            updateLastBoardTime($ipaddress);
        }
    }
    else if ($action == "output_update") {
        $ipaddress = test_input($_GET["ipaddress"]);
        $state = test_input($_GET["state"]);
        $result = updateOutput($id, $state);
        echo $result;
    }
    else {
        echo "Invalid HTTP request.";
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>