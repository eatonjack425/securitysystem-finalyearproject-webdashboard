<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 	 <link rel="stylesheet" href="/resources/demos/style.css">
  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Security Dashboard</title>
    <?php include'inc/nav.php';
    ?>
  </head>
  <body>
    <?php
    if (isset($_GET["page"]))
    {
    $page = htmlspecialchars($_GET["page"]);
    if($page == "login"){
        include'inc/login.php';
    }
    else if ($page == "EventLogView" && isset($_SESSION['perms'])){
        include'inc/EventLogView.php';
    }
    else if ($page == "gallery" && isset($_SESSION['perms'])){
        include'inc/gallery.php';
    }
    else if ($page == "liveview" && isset($_SESSION['perms'])){
        include'inc/LiveView.php';
    }
    else if ($page == "fr" && isset($_SESSION['perms'])){
        include'inc/FacialRecognition.php';
    }
    else if ($page == "records" && isset($_SESSION['perms'])){
        include'inc/view_eventlog.php';
    }
    else
        include'inc/login.php';
    }
    else
        include'inc/login.php';
?> 
  </body>
</html>
