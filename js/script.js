const imageSelect = document.getElementById('imageSelect')

Promise.all([
faceapi.nets.ssdMobilenetv1.loadFromUri('/models'),
faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
faceapi.nets.faceRecognitionNet.loadFromUri('/models')
]).then(start)

function start() {
		document.body.append('Loaded')
      const image = faceapi.bufferToImage(imageSelect.files[0])
      const detections = faceapi.detectAllFaces(image)
      .withFaceLandmarks().withFaceDescriptors()
}
        