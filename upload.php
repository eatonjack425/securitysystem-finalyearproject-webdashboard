<?php
// Rui Santos
// Complete project details at https://RandomNerdTutorials.com/esp32-cam-post-image-photo-server/
// Code Based on this example: w3schools.com/php/php_file_upload.asp
$systemID = '1';
$temperature = '20';
$results;
$target_dir = "E:/xampp/htdocs/securityDashboard/uploads/";
$save_dir = "/securityDashboard//uploads/";
$datum = mktime(date('H')+0, date('i'), date('s'), date('m'), date('d'), date('y'));
$imagePath = $target_dir . date('Y.m.d_H_i_s_', $datum) . basename($_FILES["imageFile"]["name"]);
$save_dir = $save_dir . date('Y.m.d_H_i_s_', $datum) . basename($_FILES["imageFile"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($imagePath,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    }
    else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check if file already exists
if (file_exists($imagePath)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}

// Check file size
if ($_FILES["imageFile"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
    }
    else {
        if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $imagePath)) {
            $db = "SecurityDashboardDB";
            $servername = "localhost";
            $username = "root";
            $password = "";
            $conn = new mysqli($servername, $username, $password, $db);
            
            if ($conn->connect_error) {
                die ("DB Connection Failed: ". mysqli_connect_error());
            }
            echo "DB Connected Successfully";
            $sql = "INSERT INTO eventlogs (imagePath, temperature, systemID) VALUES ('".$save_dir."', '".$temperature."', '".$systemID."');";
            
            if ($conn->query($sql) === TRUE)
            {
                echo "Insert Successful";
                echo "The file ". basename( $_FILES["imageFile"]["name"]). " has been uploaded. - ".$results;
            }
            else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            $conn->close;
        }
        else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
    ?>